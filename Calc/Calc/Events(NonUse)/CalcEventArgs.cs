﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calc.Events
{
    public class CalcEventArgs : EventArgs
    {
        public string Message { get; set; }

        public CalcEventArgs(string message)
        {
            Message = message;
        }
    }
}
