﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalcBLogic.Classes.Simple;

namespace Calc.Events
{
    public class Subscriber
    {
        public Subscriber(Publisher pub)
        {
            pub.RaiseSomeEvent += HandleSomeEvent;
        }

        private void HandleSomeEvent(object sender, CalcEventArgs e)
        {
            Console.WriteLine("Calculation succes! The result is: {0}", e.Message);
            Console.ReadKey();
            Console.Clear();
        }
    }
}
