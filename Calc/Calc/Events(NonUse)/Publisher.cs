﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calc.Events
{
    public class Publisher
    {
        public event EventHandler<CalcEventArgs> RaiseSomeEvent;

        public void DoSomething(string message)
        {
            OnRaiseSomeEvent(new CalcEventArgs(message));
        }

        protected virtual void OnRaiseSomeEvent(CalcEventArgs e)
        {
            EventHandler<CalcEventArgs> handler = RaiseSomeEvent;

            if (handler != null)
            {
                handler(this, e);
            }
        }
    }
}
