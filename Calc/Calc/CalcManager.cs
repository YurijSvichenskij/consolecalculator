﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalcBLogic.Classes.Complex;
using CalcBLogic.Classes.Simple;
using Calc.Events;
using ExtensionLibrary;

namespace CalcBLogic.Classes
{
    public class CalcManager
    {
        private MemoryFactorySimple _memoryFactorySimple;
        private MemoryFactoryComplex _memoryFactoryComplex;
        private ExecuteOperationSimple _executeOperationSimple;
        private ExecuteOperationComplex _executeOperationComplex;

        public CalcManager()
        {
            _executeOperationComplex = new ExecuteOperationComplex();
            _memoryFactoryComplex = new MemoryFactoryComplex();
            _executeOperationSimple = new ExecuteOperationSimple();
            _memoryFactorySimple = new MemoryFactorySimple();
            //_executeOperationSimple.RaiseEvent += _executeOpSimple_RaiseEvent;
            _executeOperationComplex.RaiseEvent += _executeOp_RaiseEvent;
            Menu();
        }

        private void _executeOp_RaiseEvent(object sender, EventArgs e)
        {
            var temp = (ConsoleKey)sender;
            Console.Clear();
            Console.WriteLine("Operation {0} was performed whith using of complex calculator.", temp.ToString());
            Console.ReadKey();
            Console.Clear();
        }

        private void _executeOpSimple_RaiseEvent(object sender, EventArgs e)
        {
            var temp = (ConsoleKey)sender;
            Console.Clear();
            Console.WriteLine("Operation {0} was performed whith using of simple calculator.", temp.ToString());
            Console.ReadKey();
            Console.Clear();
        }

        public void Menu()
        {
            try
            {
                var _key = new ConsoleKey();

                Console.WriteLine("Press 'ESC' to exit.");
                Console.WriteLine("Choose calculator:");
                Console.WriteLine("1) Simple calculator;");
                Console.WriteLine("2) Complex calculator.");

                _key = Console.ReadKey().Key;

                Console.Clear();

                switch (_key)
                {
                    case ConsoleKey.D1:
                        OutSimple();
                        break;
                    case ConsoleKey.NumPad1:
                        OutSimple();
                        break;
                    case ConsoleKey.D2:
                        OutComplex();
                        break;
                    case ConsoleKey.NumPad2:
                        OutComplex();
                        break;
                    case ConsoleKey.Escape:
                        Escape();
                        break;
                    default:
                        throw new Exception("No such calculator.");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                Menu();
            }
        }

        public void OutComplex()
        {
            try
            {
                var key = new ConsoleKey();
                var argReal = new double();
                var argImaginary = new double();

                Console.WriteLine("Available actions: [+][-][*][/]");
                Console.WriteLine("Press 'C' co clear.");
                Console.WriteLine("Press 'ESC' to return to menu.");
                Console.WriteLine();
                Console.WriteLine(" First complex number: {0}", _memoryFactoryComplex.Memory);
                Console.Write("{0,23}", "Action: ");

                key = Console.ReadKey().Key;

                if (key == ConsoleKey.C)
                {
                    Console.Clear();
                    _memoryFactoryComplex.Memory.Real = 0;
                    _memoryFactoryComplex.Memory.Imaginary = 0;
                    OutComplex();
                }
                if (key == ConsoleKey.Escape)
                {
                    Console.Clear();
                    Menu();
                }

                Console.WriteLine();
                Console.WriteLine("Second complex number: ");
                Console.Write("{0,23}", "Real: ");
                argReal = Convert.ToDouble(Console.ReadLine());
                Console.Write("{0,23}", "Imaginary: ");
                argImaginary = Convert.ToDouble(Console.ReadLine());

                var arg = new ComplexNumber(argReal, argImaginary);

                Console.Clear();

                _memoryFactoryComplex.Memory = _executeOperationComplex.Execute(key, _memoryFactoryComplex.Memory, arg);
                OutComplex();
            }
            catch (Exception ex)
            {
                Console.Clear();
                Console.WriteLine("Error: " + ex.Message);
                OutComplex();
            }
        }

        public void OutSimple()
        {
            try
            {
                var key = new ConsoleKey();
                var arg = new double();

                Console.WriteLine("Available actions: [+][-][*][/]");
                Console.WriteLine("Press 'C' co clear.");
                Console.WriteLine("Press 'ESC' to return to menu.");
                Console.WriteLine();
                Console.WriteLine(" First number: {0}", _memoryFactorySimple.Memory.ToBinary());
                Console.Write("{0,15}", "Action: ");

                key = Console.ReadKey().Key;

                if (key == ConsoleKey.C)
                {
                    Console.Clear();
                    _memoryFactorySimple.Memory = 0;
                    OutSimple();
                }
                if (key == ConsoleKey.Escape)
                {
                    Console.Clear();
                    Menu();
                }

                Console.WriteLine();
                Console.Write("Second number: ");
                arg = Convert.ToDouble(Console.ReadLine());

                Console.Clear();

                _memoryFactorySimple.Memory = _executeOperationSimple.Execute(key, _memoryFactorySimple.Memory, arg);
                OutSimple();
            }
            catch (Exception ex)
            {
                Console.Clear();
                Console.WriteLine("Error: " + ex.Message);
                OutSimple();
            }
        }

        public void Escape()
        {
            Console.Clear();
            Environment.Exit(0);
        }
    }
}
