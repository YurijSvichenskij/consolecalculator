﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CalcBLogic.Classes.Simple;
using Microsoft.Win32;
using System.IO;

namespace DesktopApp
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //ExecuteOperationSimple executeOperation = new ExecuteOperationSimple();

        public MainWindow()
        {
            InitializeComponent();
        }

        /*private void button_Click(object sender, RoutedEventArgs e)
        {
            InputNumber.Text += (string)button.Content;
        }

        private void button_Copy_Click(object sender, RoutedEventArgs e)
        {
            InputNumber.Text += (string)button_Copy.Content;
        }

        private void button_Copy1_Click(object sender, RoutedEventArgs e)
        {
            InputNumber.Text += (string)button_Copy1.Content;
        }

        private void button_Copy2_Click(object sender, RoutedEventArgs e)
        {
            InputNumber.Text += (string)button_Copy2.Content;
        }

        private void button_Copy3_Click(object sender, RoutedEventArgs e)
        {
            InputNumber.Text += (string)button_Copy3.Content;
        }

        private void button_Copy4_Click(object sender, RoutedEventArgs e)
        {
            InputNumber.Text += (string)button_Copy4.Content;
        }

        private void button_Copy5_Click(object sender, RoutedEventArgs e)
        {
            InputNumber.Text += (string)button_Copy5.Content;
        }

        private void button_Copy6_Click(object sender, RoutedEventArgs e)
        {
            InputNumber.Text += (string)button_Copy6.Content;
        }

        private void button_Copy7_Click(object sender, RoutedEventArgs e)
        {
            InputNumber.Text += (string)button_Copy7.Content;
        }

        private void button_Copy8_Click(object sender, RoutedEventArgs e)
        {
            InputNumber.Text += (string)button_Copy8.Content;
        }

        private void button_Copy9_Click(object sender, RoutedEventArgs e)
        {
            if (!InputNumber.Text.Equals(string.Empty))
                InputNumber.Text += (string)button_Copy9.Content;
        }

        private void Backspace_Click(object sender, RoutedEventArgs e)
        {
            if (!InputNumber.Text.Equals(string.Empty))
                InputNumber.Text = InputNumber.Text.Substring(0, InputNumber.Text.Length - 1);
        }

        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            InputNumber.Text = string.Empty;
            CurrentNumber.Content = "0";
            CurrentAction.Content = string.Empty;
        }

        private void Plus_Click(object sender, RoutedEventArgs e)
        {
            if (CurrentAction.Content.Equals(string.Empty) && InputNumber.Text.Equals(string.Empty))
            {
                CurrentAction.Content = Plus.Content;
            }
            else if (!CurrentAction.Content.Equals(string.Empty) && !InputNumber.Text.Equals(string.Empty))
            {
                var key = (string)CurrentAction.Content;
                var arg1 = Convert.ToDouble(CurrentNumber.Content);
                var arg2 = Convert.ToDouble(InputNumber.Text);
                CurrentNumber.Content = executeOperation.Execute(key, arg1, arg2);
                InputNumber.Text = string.Empty;
                CurrentAction.Content = Plus.Content;
            }
            else if (!CurrentAction.Content.Equals(string.Empty) && InputNumber.Text.Equals(string.Empty))
            {
                CurrentAction.Content = Plus.Content;
            }
            else if (CurrentAction.Content.Equals(string.Empty) && !InputNumber.Text.Equals(string.Empty))
            {
                CurrentNumber.Content = InputNumber.Text;
                CurrentAction.Content = Plus.Content;
                InputNumber.Text = string.Empty;
            }
        }

        private void Minus_Click(object sender, RoutedEventArgs e)
        {
            if (CurrentAction.Content.Equals(string.Empty) && InputNumber.Text.Equals(string.Empty))
            {
                CurrentAction.Content = Minus.Content;
            }
            else if (!CurrentAction.Content.Equals(string.Empty) && !InputNumber.Text.Equals(string.Empty))
            {
                var key = (string)CurrentAction.Content;
                var arg1 = Convert.ToDouble(CurrentNumber.Content);
                var arg2 = Convert.ToDouble(InputNumber.Text);
                CurrentNumber.Content = executeOperation.Execute(key, arg1, arg2);
                InputNumber.Text = string.Empty;
                CurrentAction.Content = Minus.Content;
            }
            else if (!CurrentAction.Content.Equals(string.Empty) && InputNumber.Text.Equals(string.Empty))
            {
                CurrentAction.Content = Minus.Content;
            }
            else if (CurrentAction.Content.Equals(string.Empty) && !InputNumber.Text.Equals(string.Empty))
            {
                CurrentNumber.Content = InputNumber.Text;
                CurrentAction.Content = Minus.Content;
                InputNumber.Text = string.Empty;
            }
        }

        private void Increace_Click(object sender, RoutedEventArgs e)
        {
            if (CurrentAction.Content.Equals(string.Empty) && InputNumber.Text.Equals(string.Empty))
            {
                CurrentAction.Content = Increace.Content;
            }
            else if (!CurrentAction.Content.Equals(string.Empty) && !InputNumber.Text.Equals(string.Empty))
            {
                var key = (string)CurrentAction.Content;
                var arg1 = Convert.ToDouble(CurrentNumber.Content);
                var arg2 = Convert.ToDouble(InputNumber.Text);
                CurrentNumber.Content = executeOperation.Execute(key, arg1, arg2);
                InputNumber.Text = string.Empty;
                CurrentAction.Content = Increace.Content;
            }
            else if (!CurrentAction.Content.Equals(string.Empty) && InputNumber.Text.Equals(string.Empty))
            {
                CurrentAction.Content = Increace.Content;
            }
            else if (CurrentAction.Content.Equals(string.Empty) && !InputNumber.Text.Equals(string.Empty))
            {
                CurrentNumber.Content = InputNumber.Text;
                CurrentAction.Content = Increace.Content;
                InputNumber.Text = string.Empty;
            }
        }

        private void Divide_Click(object sender, RoutedEventArgs e)
        {
            if (CurrentAction.Content.Equals(string.Empty) && InputNumber.Text.Equals(string.Empty))
            {
                CurrentAction.Content = Divide.Content;
            }
            else if (!CurrentAction.Content.Equals(string.Empty) && !InputNumber.Text.Equals(string.Empty))
            {
                var key = (string)CurrentAction.Content;
                var arg1 = Convert.ToDouble(CurrentNumber.Content);
                var arg2 = Convert.ToDouble(InputNumber.Text);
                CurrentNumber.Content = executeOperation.Execute(key, arg1, arg2);
                InputNumber.Text = string.Empty;
                CurrentAction.Content = Divide.Content;
            }
            else if (!CurrentAction.Content.Equals(string.Empty) && InputNumber.Text.Equals(string.Empty))
            {
                CurrentAction.Content = Divide.Content;
            }
            else if (CurrentAction.Content.Equals(string.Empty) && !InputNumber.Text.Equals(string.Empty))
            {
                CurrentNumber.Content = InputNumber.Text;
                CurrentAction.Content = Divide.Content;
                InputNumber.Text = string.Empty;
            }
        }

        private void Result_Click(object sender, RoutedEventArgs e)
        {
            if (!CurrentAction.Content.Equals(string.Empty) && !InputNumber.Text.Equals(string.Empty))
            {
                var key = (string)CurrentAction.Content;
                var arg1 = Convert.ToDouble(CurrentNumber.Content);
                var arg2 = Convert.ToDouble(InputNumber.Text);
                CurrentNumber.Content = executeOperation.Execute(key, arg1, arg2);
                InputNumber.Text = string.Empty;
                CurrentAction.Content = string.Empty;
            }
        }*/

        private void Theme_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                ResourceDictionary dict = new ResourceDictionary();
                string path = @openFileDialog.FileName;
                dict.Source = new Uri(path);
                Application.Current.Resources.MergedDictionaries.Add(dict);
            }
        }
    }
}
