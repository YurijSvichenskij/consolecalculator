﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DesktopApp.Model;
using DesktopApp.Command;
using System.Windows.Input;
using System.Windows;

namespace DesktopApp.ViewModel
{
    public class CalculatorViewModel : INotifyPropertyChanged
    {
        private CalculatorModel _calculatorModel;
        public ICommand digitButtonPress { get; set; }
        public ICommand actionButtonPress { get; set; }
        public ICommand additionalButtonPress { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;
        private string _Memory;
        public string Memory
        {
            get
            {
                return _Memory;
            }
            set
            {
                _Memory = value;
                OnPropertyChanged("Memory");
            }
        }
        private string _action;
        public string Action
        {
            get
            {
                return _action;
            }
            set
            {
                _action = value;
                OnPropertyChanged("Action");
            }
        }
        private string _inputNumber;
        public string InputNumber
        {
            get
            {
                return _inputNumber;
            }
            set
            {
                _inputNumber = value;
                OnPropertyChanged("InputNumber");
            }
        }

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public CalculatorViewModel()
        {
            _calculatorModel = new CalculatorModel();
            digitButtonPress = new RelayCommand(DigitButtonPress);
            actionButtonPress = new RelayCommand(ActionButtonPress);
            additionalButtonPress = new RelayCommand(AdditionalButtonPress);
            InputNumber = string.Empty;
            Action = string.Empty;
            Memory = "0";
        }

        public void DigitButtonPress(object param)
        {
            if (param.ToString().Equals(".") && InputNumber.Equals(string.Empty)) { }
            else
                InputNumber += param.ToString();
        }

        public void AdditionalButtonPress(object param)
        {
            if (param.ToString().Equals("C"))
            {
                Memory = "0";
                InputNumber = string.Empty;
                Action = string.Empty;
            }
            if (param.ToString().Equals("Del"))
            {
                if (!InputNumber.Equals(string.Empty))
                    InputNumber = InputNumber.Substring(0, InputNumber.Length - 1);
            }
        }

        public void ActionButtonPress(object param)
        {
            if (param.ToString().Equals("=") && !InputNumber.Equals(string.Empty) && !Action.Equals(string.Empty))
            {
                Memory = _calculatorModel.ExecuteOperation.Execute(Action, Convert.ToDouble(Memory), Convert.ToDouble(InputNumber)).ToString();
                InputNumber = string.Empty;
                Action = string.Empty;
            }
            else if (!Action.Equals(param.ToString()) && !Action.Equals(string.Empty) && !InputNumber.Equals(string.Empty))
            {
                Memory = _calculatorModel.ExecuteOperation.Execute(Action, Convert.ToDouble(Memory), Convert.ToDouble(InputNumber)).ToString();
                Action = param.ToString();
                InputNumber = string.Empty;
            }
            else if (Action.Equals(param.ToString()) && !Action.Equals(string.Empty) && !InputNumber.Equals(string.Empty))
            {
                Memory = _calculatorModel.ExecuteOperation.Execute(Action, Convert.ToDouble(Memory), Convert.ToDouble(InputNumber)).ToString();
                Action = param.ToString();
                InputNumber = string.Empty;
            }
            else if (Action.Equals(string.Empty) && !InputNumber.Equals(string.Empty))
            {
                Action = param.ToString();
                Memory = InputNumber;
                InputNumber = string.Empty;
            }
            else if (InputNumber.Equals(string.Empty))
                Action = param.ToString();
        }
    }
}
