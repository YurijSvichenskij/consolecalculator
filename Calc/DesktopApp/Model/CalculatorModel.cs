﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalcBLogic.Classes.Simple;

namespace DesktopApp.Model
{
    public class CalculatorModel
    {
        public MemoryFactorySimple MemoryFactory { get; set; }
        public ExecuteOperationSimple ExecuteOperation { get; set; }

        public CalculatorModel()
        {
            MemoryFactory = new MemoryFactorySimple();
            ExecuteOperation = new ExecuteOperationSimple();
            MemoryFactory.Memory = 0.0;
            MemoryFactory.Action = string.Empty;
        }
    }
}
