﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtensionLibrary
{
    public static class ExtensionsDigit
    {
        public static string ToBinary(this double num)
        {
            char[] splitArray = { '.', ',' };

            var arraySplitted = num.ToString().Split(splitArray);

            if (arraySplitted.Length == 1)
            {
                int intRepresantationFirstPart = Convert.ToInt32(arraySplitted[0]);
                string binaryRepresantationFirstPart = Convert.ToString(intRepresantationFirstPart, 2);
                return string.Format("{1} - {0}", binaryRepresantationFirstPart, num.ToString());
            }
            else
            {
                int intRepresantationFirstPart = Convert.ToInt32(arraySplitted[0]);
                int intRepresantationSecondPart = Convert.ToInt32(arraySplitted[1]);

                string binaryRepresantationFirstPart = Convert.ToString(intRepresantationFirstPart, 2);
                string binaryRepresantationSecondPart = Convert.ToString(intRepresantationSecondPart, 2);

                return string.Format("{2} - {0}.{1}", binaryRepresantationFirstPart, binaryRepresantationSecondPart, num.ToString());
            }
        }

        public static string ToHex(this double num)
        {
            char[] splitArray = { '.', ',' };

            var arraySplitted = num.ToString().Split(splitArray);

            if (arraySplitted.Length == 1)
            {
                int intRepresantationFirstPart = Convert.ToInt32(arraySplitted[0]);
                string hexRepresantationFirstPart = intRepresantationFirstPart.ToString("X");
                return string.Format("{1} - {0}", hexRepresantationFirstPart, num.ToString());
            }
            else
            {
                int intRepresantationFirstPart = Convert.ToInt32(arraySplitted[0]);
                int intRepresantationSecondPart = Convert.ToInt32(arraySplitted[1]);

                string hexRepresantationFirstPart = intRepresantationFirstPart.ToString("X");
                string hexRepresantationSecondPart = intRepresantationSecondPart.ToString("X");

                return string.Format("{2} - {0}.{1}", hexRepresantationFirstPart, hexRepresantationSecondPart, num.ToString());
            }
        }
    }
}
