﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcBLogic.Interfaces
{
    interface IExecuteOperation<T>
    {
        T Execute(ConsoleKey key, T arg1, T arg2);
    }
}
