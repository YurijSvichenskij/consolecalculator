﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcBLogic.Interfaces
{
    public interface IOperand<T>
    {
        T Add(params T[] args);
        T Min(params T[] args);
        T Inc(params T[] args);
        T Div(params T[] args);
    }
}
