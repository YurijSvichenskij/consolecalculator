﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;

namespace CalcBLogic.Interfaces
{
    interface IOperation
    {
        void Out();

        void Res(ConsoleKey _arg1, double _arg2);

        void Add(double _arg);

        void Min(double _arg);

        void Inc(double _arg);

        void Div(double _arg);

        void Clear();

        void Escape();
    }
}
