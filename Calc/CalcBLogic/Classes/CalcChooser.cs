﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcBLogic.Classes
{
    public class CalcChooser
    {
        private ComplexOperation _calcComplex;
        private Operation _calc;

        public CalcChooser()
        {
            try
            {
                var _key = new ConsoleKey();

                Console.WriteLine("Choose calculator:");
                Console.WriteLine("1) Simple calculator;");
                Console.WriteLine("2) Complex calculator.");

                _key = Console.ReadKey().Key;

                Console.Clear();

                switch (_key)
                {
                    case ConsoleKey.D1:
                        _calc = new Operation();
                        break;
                    case ConsoleKey.NumPad1:
                        _calc = new Operation();
                        break;
                    case ConsoleKey.D2:
                        _calcComplex = new ComplexOperation();
                        break;
                    case ConsoleKey.NumPad2:
                        _calcComplex = new ComplexOperation();
                        break;
                    default:
                        throw new Exception("No such calculator.");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
                new CalcChooser();
            }
        }

        ~CalcChooser() { }
    }
}
