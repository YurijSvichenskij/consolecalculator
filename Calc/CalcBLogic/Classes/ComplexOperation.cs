﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalcBLogic.Classes;

namespace CalcBLogic.Classes
{
    public class ComplexOperation
    {
        private ComplexNumber _memory;
        private delegate void OperationDelegate(ComplexNumber _arg);
        private Dictionary<ConsoleKey, OperationDelegate> _operations;

        public ComplexOperation()
        {
            _operations =
                new Dictionary<ConsoleKey, OperationDelegate>
                {
                    { ConsoleKey.Add, this.Add },
                    { ConsoleKey.Subtract, this.Min },
                    { ConsoleKey.Multiply, this.Inc },
                    { ConsoleKey.Divide, this.Div },
                };

            _memory = new ComplexNumber();

            Out();
        }

        public void Res(ConsoleKey _key, ComplexNumber _arg)
        {
            if (!_operations.ContainsKey(_key))
                throw new Exception(String.Format("{0} is unknown operation.", _key.ToString()));

            _operations[_key](_arg);
        }

        public void Out()
        {
            try
            {
                var _key = new ConsoleKey();
                var _argReal = new double();
                var _argImaginary = new double();

                Console.WriteLine("Available actions: [+][-][*][/]");
                Console.WriteLine("Press 'C' co clear.");
                Console.WriteLine("Press 'ESC' to exit.");
                Console.WriteLine();
                Console.WriteLine(" First complex number: {0}", _memory);
                Console.Write("{0,23}", "Action: ");

                _key = Console.ReadKey().Key;

                if (_key == ConsoleKey.C)
                {
                    Clear();
                }
                if (_key == ConsoleKey.Escape)
                {
                    Escape();
                }

                Console.WriteLine();
                Console.WriteLine("Second complex number: ");
                Console.Write("{0,23}", "Real: ");
                _argReal = Convert.ToDouble(Console.ReadLine());
                Console.Write("{0,23}", "Imaginary: ");
                _argImaginary = Convert.ToDouble(Console.ReadLine());

                var _arg = new ComplexNumber(_argReal, _argImaginary);

                Console.Clear();

                Res(_key, _arg);
                Out();
            }
            catch (Exception ex)
            {
                Console.Clear();
                Console.WriteLine("Error: " + ex.Message);
                Out();
            }

        }

        public void Add(ComplexNumber _arg)
        {
            _memory = _memory + _arg;
        }

        public void Min(ComplexNumber _arg)
        {
            _memory = _memory - _arg;
        }

        public void Inc(ComplexNumber _arg)
        {
            _memory = _memory * _arg;
        }

        public void Div(ComplexNumber _arg)
        {
            _memory = _memory / _arg;
        }

        public void Clear()
        {
            _memory.Real = 0;
            _memory.Imaginary = 0;
            Console.Clear();
            Out();
        }

        public void Escape()
        {
            Console.Clear();
            Environment.Exit(0);
        }

        ~ComplexOperation() { }
    }

}
