﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcBLogic.Classes.Simple
{
    public class MemoryFactorySimple
    {
        private double _memory;
        public string Action { get; set; }

        public MemoryFactorySimple()
        {
            _memory = 0;
            Action = string.Empty;
        }

        public double Memory
        {
            get { return _memory; }
            set { _memory = value; }
        }
    }
}
