﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalcBLogic.Interfaces;

namespace CalcBLogic.Classes.Simple
{
    public class ExecuteOperationSimple : IExecuteOperation<double>
    {
        private OperationFactorySimple _operationFactory;
        /*public event EventHandler RaiseEvent;

        public void DoSomething(object sender)
        {
            OnRaiseEvent(sender, new EventArgs());
        }

        protected virtual void OnRaiseEvent(object sender, EventArgs e)
        {
            EventHandler handler = RaiseEvent;
            if (handler != null)
            {
                handler(sender, e);
            }
        }*/

        public ExecuteOperationSimple()
        {
            _operationFactory = new OperationFactorySimple();
        }

        public double Execute(ConsoleKey key, double arg1, double arg2)
        {
            //DoSomething(key);
            return _operationFactory.OperationSelect(key, arg1, arg2);
        }

        public double Execute(string key, double arg1, double arg2)
        {
            //DoSomething(key);
            return _operationFactory.OperationSelect(key, arg1, arg2);
        }
    }
}
