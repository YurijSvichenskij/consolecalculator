﻿using System;
using System.Collections.Generic;
using ExtensionOperands;

namespace CalcBLogic.Classes.Simple
{
    class OperationFactorySimple
    {
        private delegate double OperationDelegate(params double[] args);
        private Dictionary<ConsoleKey, OperationDelegate> _operations;
        private Dictionary<string, OperationDelegate> _operationsWPF;
        private OperandSimple _operandSimple;

        public OperationFactorySimple()
        {
            _operandSimple = new OperandSimple();

            _operations =
                new Dictionary<ConsoleKey, OperationDelegate>
                {
                    { ConsoleKey.Add, _operandSimple.Add },
                    { ConsoleKey.Subtract, _operandSimple.Min },
                    { ConsoleKey.Multiply, _operandSimple.Inc },
                    { ConsoleKey.Divide, _operandSimple.Div },
                };

            _operationsWPF =
                new Dictionary<string, OperationDelegate>
                {
                    { "+", _operandSimple.Add },
                    { "-", _operandSimple.Min },
                    { "*", _operandSimple.Inc },
                    { "/", _operandSimple.Div },
                };
        }

        public double OperationSelect(ConsoleKey key, double arg1, double arg2)
        {
            if (!_operations.ContainsKey(key))
                throw new Exception(String.Format("{0} is unknown operation.", key.ToString()));
            return _operations[key](arg1, arg2);
        }

        public double OperationSelect(string key, double arg1, double arg2)
        {
            if (!_operationsWPF.ContainsKey(key))
                throw new Exception(String.Format("{0} is unknown operation.", key.ToString()));
            return _operationsWPF[key](arg1, arg2);
        }
    }
}
