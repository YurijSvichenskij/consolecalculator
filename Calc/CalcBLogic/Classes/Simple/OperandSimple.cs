﻿using CalcBLogic.Interfaces;
using System;

namespace CalcBLogic.Classes.Simple
{
    public class OperandSimple : IOperand<double>
    {
        public double Add(params double[] args)
        {
            return args[0]+args[1];
        }

        public double Div(params double[] args)
        {
            //if (args[1] == 0) throw new DivideByZeroException();
            return args[0] / args[1];
        }

        public double Inc(params double[] args)
        {
            return args[0] * args[1];
        }

        public double Min(params double[] args)
        {
            return args[0] - args[1];
        }
    }
}
