﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcBLogic.Classes
{
    public class Operation : CalcBLogic.Interfaces.IOperation
    {
        private double _memory;
        private delegate void OperationDelegate(double _arg);
        private Dictionary<ConsoleKey, OperationDelegate> _operations;

        public Operation()
        {
            _operations =
                new Dictionary<ConsoleKey, OperationDelegate>
		        {
			        { ConsoleKey.Add, this.Add },
			        { ConsoleKey.Subtract, this.Min },
			        { ConsoleKey.Multiply, this.Inc },
			        { ConsoleKey.Divide, this.Div },
		        };

            _memory = 0;

            Out();
        }

        public void Res(ConsoleKey _key, double _arg)
        {
            if (!_operations.ContainsKey(_key))
                throw new Exception(String.Format("{0} is unknown action.", _key.ToString()));
            _operations[_key](_arg);
        }

        public void Out()
        {
            try
            {
                var _key = new ConsoleKey();
                var _arg = new double();
                Console.WriteLine("Available actions: [+][-][*][/]");
                Console.WriteLine("Press 'C' co clear.");
                Console.WriteLine("Press 'ESC' to exit.");
                Console.WriteLine();

                Console.WriteLine(" First number: {0:F4}", _memory);

                Console.Write("{0,15}", "Action: ");

                _key = Console.ReadKey().Key;

                if (_key == ConsoleKey.C)
                {
                    Clear();
                }
                if (_key == ConsoleKey.Escape)
                {
                    Escape();
                }

                Console.WriteLine();
                Console.Write("Second number: ");
                _arg = Convert.ToDouble(Console.ReadLine());

                Console.Clear();

                Res(_key, _arg);
            }
            catch (Exception ex)
            {
                Console.Clear();
                Console.WriteLine("Error: " + ex.Message);
                Out();
            }
        }

        public void Add(double _arg)
        {
            _memory = _memory + _arg;
            Out();
        }

        public void Min(double _arg)
        {
            _memory = _memory - _arg;
            Out();
        }

        public void Inc(double _arg)
        {
            _memory = _memory * _arg;
            Out();
        }

        public void Div(double _arg)
        {
            if (_arg == 0)
            {
                throw new DivideByZeroException();
            }
            _memory = _memory / _arg;
            Out();
        }

        public void Clear()
        {
            _memory = 0;
            Console.Clear();
            Out();
        }

        public void Escape()
        {
            Console.Clear();
            Environment.Exit(0);
        }

        ~Operation(){}
    }
}
