﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcBLogic.Classes.Complex
{
    public class MemoryFactoryComplex
    {
        private ComplexNumber _memory;

        public MemoryFactoryComplex()
        {
            _memory = new ComplexNumber();
        }

        public ComplexNumber Memory
        {
            get { return _memory; }
            set { _memory = value; }
        }
    }
}
