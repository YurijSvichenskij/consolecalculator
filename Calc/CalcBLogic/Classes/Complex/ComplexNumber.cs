﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcBLogic.Classes
{
    public class ComplexNumber : IEquatable<ComplexNumber>
    {
        private double _real;
        private double _imaginary;

        public double Real
        {
            get { return _real; }
            set { _real = value; }
        }

        public double Imaginary
        {
            get { return _imaginary; }
            set { _imaginary = value; }
        }

        public ComplexNumber()
        {
            Real = 0;
            Imaginary = 0;
        }

        public ComplexNumber(double _real, double _imaginary)
        {
            Real = _real;
            Imaginary = _imaginary;
        }

        public override string ToString()
        {
            return (String.Format("{0} + {1}i", Real, Imaginary));
        }

        public bool Equals(ComplexNumber other)
        {
            return (this.Real == other.Real && this.Imaginary == other.Imaginary);
        }

        public static ComplexNumber operator +(ComplexNumber a, ComplexNumber b)
        {
            return new ComplexNumber(a.Real + b.Real, a.Imaginary + b.Imaginary);
        }

        public static ComplexNumber operator -(ComplexNumber a, ComplexNumber b)
        {
            return new ComplexNumber(a.Real - b.Real, a.Imaginary - b.Imaginary);
        }

        public static ComplexNumber operator *(ComplexNumber a, ComplexNumber b)
        {
            return new ComplexNumber((a.Real * b.Real - a.Imaginary * b.Imaginary), (a.Real * b.Imaginary + a.Imaginary * b.Real));
        }

        public static ComplexNumber operator /(ComplexNumber a, ComplexNumber b)
        {
            if (b.Real == 0)
            {
                throw new DivideByZeroException();
            }

            double _arg1 = (a.Real * b.Real + a.Imaginary * b.Imaginary) / (Math.Pow(b.Real, 2) + Math.Pow(b.Imaginary, 2));
            double _arg2 = (a.Imaginary * b.Real - a.Real * b.Imaginary) / (Math.Pow(b.Real, 2) + Math.Pow(b.Imaginary, 2));
            return new ComplexNumber(_arg1, _arg2);
        }

        public static bool operator ==(ComplexNumber arg1, ComplexNumber arg2)
        {
            return arg1.Equals(arg2);
        }

        public static bool operator !=(ComplexNumber arg1, ComplexNumber arg2)
        {
            return !arg1.Equals(arg2);
        }

        public static bool operator >(ComplexNumber arg1, ComplexNumber arg2)
        {
            double first = Math.Sqrt(Math.Pow(arg1.Real, 2) + Math.Pow(arg1.Imaginary, 2));
            double second = Math.Sqrt(Math.Pow(arg2.Real, 2) + Math.Pow(arg2.Imaginary, 2));

            return first > second;
        }

        public static bool operator <(ComplexNumber arg1, ComplexNumber arg2)
        {
            double first = Math.Sqrt(Math.Pow(arg1.Real, 2) + Math.Pow(arg1.Imaginary, 2));
            double second = Math.Sqrt(Math.Pow(arg2.Real, 2) + Math.Pow(arg2.Imaginary, 2));

            return first < second;
        }

        public static bool operator >=(ComplexNumber arg1, ComplexNumber arg2)
        {
            double first = Math.Sqrt(Math.Pow(arg1.Real, 2) + Math.Pow(arg1.Imaginary, 2));
            double second = Math.Sqrt(Math.Pow(arg2.Real, 2) + Math.Pow(arg2.Imaginary, 2));

            return first >= second;
        }

        public static bool operator <=(ComplexNumber arg1, ComplexNumber arg2)
        {
            double first = Math.Sqrt(Math.Pow(arg1.Real, 2) + Math.Pow(arg1.Imaginary, 2));
            double second = Math.Sqrt(Math.Pow(arg2.Real, 2) + Math.Pow(arg2.Imaginary, 2));

            return first <= second;
        }
    }
}
