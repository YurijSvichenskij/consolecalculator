﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcBLogic.Classes.Complex
{
    class OperationFactoryComplex
    {
        private delegate ComplexNumber OperationDelegate(params ComplexNumber[] args);
        private Dictionary<ConsoleKey, OperationDelegate> _operations;
        private OperandComplex _operandComplex;

        public OperationFactoryComplex()
        {
            _operandComplex = new OperandComplex();

            _operations =
                new Dictionary<ConsoleKey, OperationDelegate>
                {
                    { ConsoleKey.Add, _operandComplex.Add },
                    { ConsoleKey.Subtract, _operandComplex.Min },
                    { ConsoleKey.Multiply, _operandComplex.Inc },
                    { ConsoleKey.Divide, _operandComplex.Div },
                };
        }

        public ComplexNumber OperationSelect(ConsoleKey key, ComplexNumber arg1, ComplexNumber arg2)
        {
            if (!_operations.ContainsKey(key))
                throw new Exception(String.Format("{0} is unknown operation.", key.ToString()));
            return _operations[key](arg1, arg2);
        }
    }
}
