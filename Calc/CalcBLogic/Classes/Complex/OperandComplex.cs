﻿using CalcBLogic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcBLogic.Classes.Complex
{
    public class OperandComplex : IOperand<ComplexNumber>
    {
        public ComplexNumber Add(params ComplexNumber[] args)
        {
            return args[0] + args[1];
        }

        public ComplexNumber Min(params ComplexNumber[] args)
        {
            return args[0] - args[1];
        }

        public ComplexNumber Div(params ComplexNumber[] args)
        {
            return args[0] / args[1];
        }

        public ComplexNumber Inc(params ComplexNumber[] args)
        {
            return args[0] * args[1];
        }
    }
}
