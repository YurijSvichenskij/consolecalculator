﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalcBLogic.Interfaces;

namespace CalcBLogic.Classes.Complex
{
    public class ExecuteOperationComplex : IExecuteOperation<ComplexNumber>
    {
        private OperationFactoryComplex _operationFactory;
        public event EventHandler RaiseEvent;

        public void DoSomething(object sender)
        {
            OnRaiseEvent(sender, new EventArgs());
        }

        protected virtual void OnRaiseEvent(object sender, EventArgs e)
        {
            EventHandler handler = RaiseEvent;
            if (handler != null)
            {
                handler(sender, e);
            }
        }

        public ExecuteOperationComplex()
        {
            _operationFactory = new OperationFactoryComplex();
        }

        public ComplexNumber Execute(ConsoleKey key, ComplexNumber arg1, ComplexNumber arg2)
        {
            DoSomething(key);
            return _operationFactory.OperationSelect(key, arg1, arg2);
        }
    }
}
