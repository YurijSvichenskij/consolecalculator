﻿using System;
using CalcBLogic.Classes.Simple;

namespace ExtensionOperands
{
    public static class ExtensionsOperandSimple
    {
        public static string Add(this OperandSimple os,  string[] args)
        {
            var first = Convert.ToInt32(args[0], 2);
            var second = Convert.ToInt32(args[0], 2);

            return (first + second).ToString();
        }

        public static string Div(this OperandSimple os, string[] args)
        {
            var first = Convert.ToInt32(args[0], 2);
            var second = Convert.ToInt32(args[0], 2);

            return (first / second).ToString();
        }

        public static string Inc(this OperandSimple os, string[] args)
        {
            var first = Convert.ToInt32(args[0], 2);
            var second = Convert.ToInt32(args[0], 2);

            return (first * second).ToString();
        }

        public static string Min(this OperandSimple os, string[] args)
        {
            var first = Convert.ToInt32(args[0], 2);
            var second = Convert.ToInt32(args[0], 2);

            return (first - second).ToString();
        }
    }
}
